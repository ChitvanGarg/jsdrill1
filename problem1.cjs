
const problem1 = function(inventory, id){
    if(inventory != null && Array.isArray(inventory) && id != null){
        for(i=0; i<inventory.length; i++){
            if(inventory[i].id == id){
                return inventory[i];
            }
        }
    }
    
    return [];
    
}

module.exports = problem1

