const cars_inventory = require('../inventory.cjs');
const problem1 = require('../problem1.cjs');

const result = problem1(cars_inventory, 33);

console.log(result)

// if(result != undefined && !Array.isArray(result) && Object.keys(result).length != 0) 
console.log(`Car 33 is a ${result.car_year} ${result.car_make} ${result.car_model}`)
